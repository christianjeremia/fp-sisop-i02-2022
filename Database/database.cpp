#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 4443

struct allowed{
	char name[10000];
	char password[10000];
};

struct allowed_database{
	char database[10000];
	char name[10000];
};

struct table{
	int jumlahkolom;
	char type[100][10000];
	char data[100][10000];
};

void createUser(char *nama, char *password){
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"databases/user.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekUserExist(char *username ){
	FILE *fp;
	struct allowed user;
	int id,found=0;
	fp=fopen("../database/databases/user.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		
		if(strcmp(user.name, username)==0){
			return 1;
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);

	return 0;
	
}

void insertPermission(char *nama, char *database){
	struct allowed_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[]={"databases/permission.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekAllowedDatabase(char *nama, char *database){
	FILE *fp;
	struct allowed_database user;
	int id,found=0;
	printf("nama = %s  database = %s", nama, database);
	fp=fopen("../database/databases/permission.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, nama)==0){
		
			if(strcmp(user.database, database)==0){
				return 1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}

int findColumn(char *table, char *kolom){
	FILE *fp;
	struct table user;
	int id,found=0;
	fp=fopen(table,"rb");	
	fread(&user,sizeof(user),1,fp);
   
	int index=-1;
	for(int i=0; i < user.jumlahkolom; i++){
      
		if(strcmp(user.data[i], kolom)==0){
			index = i;
		}
	}
    if(feof(fp)){
		return -1;
	}
	fclose(fp);
	return index;
}

int deleteColumn(char *table, int index){
	FILE *fp, *fp1;
	struct table user;
	int id,found=0;
	fp=fopen(table,"rb");
	fp1=fopen("temp","wb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
		struct table userCopy;
		int iterasi=0;
		for(int i=0; i< user.jumlahkolom; i++){
			if(i == index){
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
            
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom-1;
		fwrite(&userCopy,sizeof(userCopy),1,fp1);
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteTable(char *table, char *namaTable){
    FILE *fp, *fp1;
	struct table user;
	int id,found=0;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");	
	fread(&user,sizeof(user),1,fp);
	
	int index=-1;
    struct table userCopy;
	for(int i=0; i < user.jumlahkolom; i++){
        strcpy(userCopy.data[i], user.data[i]);
        strcpy(userCopy.type[i], user.type[i]);
	}
    userCopy.jumlahkolom = user.jumlahkolom;
    fwrite(&userCopy,sizeof(userCopy),1,fp1);
    fclose(fp);
	fclose(fp1);
    remove(table);
	rename("temp", table);
	return 1;
}

int updateColumn(char *table, int index, char *ganti){
    FILE *fp, *fp1;
	struct table user;
	int id,found=0;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(1){	
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            printf("%s\n", userCopy.data[iterasi]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            printf("%s\n", userCopy.data[iterasi]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fp1);
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int updateColumnWhere(char *table, int index ,char *ganti, int indexGanti ,char *where){
    FILE *fp, *fp1;
	struct table user;
	int id,found=0;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(1){	
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[indexGanti], where)==0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            printf("%s\n", userCopy.data[iterasi]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            printf("%s\n", userCopy.data[iterasi]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fp1);
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteTableWhere(char *table, int index, char *kolom, char *where){
    FILE *fp, *fp1;
	struct table user;
	int id,found=0;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(1){	
        found = 0;
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[i], where)==0){
                found = 1;
            }
            strcpy(userCopy.data[iterasi], user.data[i]);
            printf("%s\n", userCopy.data[iterasi]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            printf("%s\n", userCopy.data[iterasi]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        if(found != 1){
            fwrite(&userCopy,sizeof(userCopy),1,fp1);
        }
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

void writelog(char *perintah, char *nama){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);
	
	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

int main(){

	int sockfd, ret;
	 struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Server Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Error in binding.\n");
		exit(1);
	}
	printf("[+]Bind to port %d\n", 4444);

	if(listen(sockfd, 10) == 0){
		printf("[+]Listening....\n");
	}else{
		printf("[-]Error in binding.\n");
	}
